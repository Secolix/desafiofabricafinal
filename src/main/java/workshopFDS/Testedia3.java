package workshopFDS;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Testedia3 {
	
	private static String url = "https://testpages.herokuapp.com/styled/basic-html-form-test.html";
	private static WebDriver driver = null;

	
	@Test
	public void fluxoFeliz() {
		
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.get("https://testpages.herokuapp.com/styled/basic-html-form-test.html");
		
		WebElement userName = driver.findElement(By.name("username"));
		userName.sendKeys("QA User");
		
		WebElement password = driver.findElement(By.name("password"));
		password.sendKeys("QA Password");
		
		WebElement comment = driver.findElement(By.name("comments"));
		comment.clear();
		comment.sendKeys("Comentario");
		
		WebElement checkbox1 = driver.findElement(By.xpath("//input[@value='cb1']"));
		WebElement checkbox2 = driver.findElement(By.xpath("//input[@value='cb2']"));
		checkbox1.click();
		checkbox2.click();
		
		WebElement radio1 = driver.findElement(By.xpath("//input[@value='rd1']"));
		radio1.click();
		
		WebElement multipleSelect = driver.findElement(By.name("multipleselect[]"));
		Select selectMultipleSelect = new Select(multipleSelect);
		selectMultipleSelect.selectByValue("ms1");
		
		WebElement multipleSelect1 = driver.findElement(By.name("multipleselect[]"));
		Select selectMultipleSelect1 = new Select(multipleSelect1);
		selectMultipleSelect1.deselectByValue("ms4");
			
		WebElement dropDown = driver.findElement(By.name("dropdown"));
		Select select = new Select(dropDown);
		select.selectByValue("dd1");
		
		WebElement submitButton = driver.findElement(By.xpath("//input[@value='submit']"));
		submitButton.click();
		
		Assert.assertEquals("QA User", driver.findElement(By.id("_valueusername")).getText());
		Assert.assertEquals("QA Password", driver.findElement(By.id("_valuepassword")).getText());
		Assert.assertEquals("Comentario", driver.findElement(By.id("_valuecomments")).getText());
		Assert.assertEquals("cb1", driver.findElement(By.id("_valuecheckboxes0")).getText());
		Assert.assertEquals("cb2", driver.findElement(By.id("_valuecheckboxes1")).getText());
		Assert.assertEquals("cb3", driver.findElement(By.id("_valuecheckboxes2")).getText());
		Assert.assertEquals("rd1", driver.findElement(By.id("_valueradioval")).getText());
		Assert.assertEquals("ms1", driver.findElement(By.id("_valuemultipleselect0")).getText());
		Assert.assertEquals("dd1", driver.findElement(By.id("_valuedropdown")).getText());
		Assert.assertEquals("submit", driver.findElement(By.id("_valuesubmitbutton")).getText());
		driver.quit();
	}
	
	
	@Test
	public void fluxoAlternativo() {
		
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.get("https://testpages.herokuapp.com/styled/basic-html-form-test.html");
		
		WebElement userName = driver.findElement(By.name("username"));
		userName.clear();
		
		WebElement password = driver.findElement(By.name("password"));
		password.clear();
		
		WebElement comment = driver.findElement(By.name("comments"));
		comment.clear();
				
		WebElement radio1 = driver.findElement(By.xpath("//input[@value='rd1']"));
		radio1.click();
		
		WebElement multipleSelect = driver.findElement(By.name("multipleselect[]"));
		Select selectMultipleSelect = new Select(multipleSelect);
		selectMultipleSelect.selectByValue("ms1");
		
		WebElement multipleSelect1 = driver.findElement(By.name("multipleselect[]"));
		Select selectMultipleSelect1 = new Select(multipleSelect1);
		selectMultipleSelect1.deselectByValue("ms4");
			
		WebElement dropDown = driver.findElement(By.name("dropdown"));
		Select select = new Select(dropDown);
		select.selectByValue("dd1");
		
		WebElement submitButton = driver.findElement(By.xpath("//input[@value='submit']"));
		submitButton.click();
		
		Assert.assertEquals("No Value for username", driver.findElement(By.xpath("//body/div/div[3]/p[1]/strong")).getText());
		Assert.assertEquals("No Value for password", driver.findElement(By.xpath("//body/div/div[3]/p[2]/strong")).getText());
		Assert.assertEquals("No Value for comments", driver.findElement(By.xpath("//body/div/div[3]/p[3]/strong")).getText());
		Assert.assertEquals("cb3", driver.findElement(By.id("_valuecheckboxes0")).getText());
		Assert.assertEquals("rd1", driver.findElement(By.id("_valueradioval")).getText());
		Assert.assertEquals("ms1", driver.findElement(By.id("_valuemultipleselect0")).getText());
		Assert.assertEquals("dd1", driver.findElement(By.id("_valuedropdown")).getText());
		Assert.assertEquals("submit", driver.findElement(By.id("_valuesubmitbutton")).getText());
		driver.quit();
	}
	
}
